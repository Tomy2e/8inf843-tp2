﻿var mongoose = require('mongoose');
var CityService = require('../services/city');

/**
 * Connexion à MongoDB en utilisant Mongoose
 */
mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 }
}).catch(function (error) {
    // An error has occured during first connection
    console.error(error);
    process.exit();
});

//mongoose.set('debug', true);
mongoose.set('useFindAndModify', false);

var db = mongoose.connection;
/**
 * En cas d'erreur
 */
db.on('error', console.error.bind(console, 'connection error:'));

/**
 * Lorsque la connexion est ouverte la première fois
 */
db.once('open', async function () {
    // we're connected!
    console.log("MongoDB connected!");
    console.log("Now importing cities...");
    if(await CityService.importFromCsvIfNecessary(__dirname + '/villes_france.csv')) {
        console.log("Cities import done!");
    } else {
        console.log("Cities import not required.");
    }
});

/**
 * En cas de reconnexion (après une déconnexion)
 */
db.on('reconnected', function () {
    // reconnection
    console.log('MongoDB reconnected!');
});

/**
 * Déconnexion
 */
db.on("disconnected", () => {
    console.log("Connection Disconnected");
});

/**
 * Connexion fermée
 */
db.on("close", () => {
    console.log("Connection Closed");
});

/**
 * Permet d'attendre que la connexion soit effectuée
 */
exports.ensureConnection = function () {
    return new Promise(function(resolve, reject) {
        if (db.readyState === 1) {
            // Already connected
            resolve();
        } else {
            // Still connecting
            mongoose.connection.on('connected', function () {
                resolve();
            });
            db.on('error', function () {
                reject();
            });
        }
    });
}

/**
 * Supprime la base de données (dangereux)
 */
exports.cleanDatabase = function () {
        return mongoose.connection.db.dropDatabase()
}
