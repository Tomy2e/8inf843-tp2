const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
    firstName : { type: String, required: true },
    lastName: { type: String, required: true },
    mail: { type: String, required: true, unique: true, index: true },
    password: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    credit: { type: Number, required: true, default: 0 },
    profilePicName: { type: String, required: true },
    image : {type: String, required:false},
    ratings: [{
        from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        note: String,
        rating: Number,
    }],
}, { timestamps: true } );

const User = mongoose.model('User', userSchema);

module.exports = User;
