const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const travelSchema = Schema({
    host: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    passengers: [{user: {type: mongoose.Schema.Types.ObjectId, ref: 'User' }, places: Number }],
    departure: { type: mongoose.Schema.Types.ObjectId, ref: 'City', required: true },
    arrival: { type: mongoose.Schema.Types.ObjectId, ref: 'City', required: true },
    addressDeparture: { type: String, required: true },
    addressArrival: { type: String, required: true },
    indications: String,
    departureTime: { type: Date, required: true },
    pricePerPassenger: { type: Number, required: true },
    places: { type: Number, required: true },
    car: { type: mongoose.Schema.Types.ObjectId, ref: 'Car' },
    travelDone: { type: Boolean, required: true, default: false},
}, { timestamps: true } );

const Travel = mongoose.model('Travel', travelSchema);

module.exports = Travel;
