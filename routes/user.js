var express = require('express');
var router = express.Router();
var UserService = require('../services/user');
const { body, validationResult } = require('express-validator');

/* GET home page. */
router.get('/login', function (req, res, next) {
    res.render('login', { title: 'Connexion - TravelExpress', auth: req.auth });
});

router.post('/login', [
    body('mail')
        .exists({ checkFalsy: true })
        .withMessage("Vous devez renseigner votre adresse courriel")
        .isEmail()
        .withMessage("L'identifiant doit être une adresse courriel valide"),
    body('password').exists(),
], async function (req, res, next) {
    try {
        var user = null;
        const errors = validationResult(req);
        var authError = false;

        if (errors.isEmpty()) user = await UserService.getUserByMail(req.body.mail);

        // User not found or password not matching?
        if (user === null || (user !== null && await UserService.checkPassword(user, req.body.password) === false)) {
            // Login Error
            authError = true;
        }

        if (authError || !errors.isEmpty()) {
            // Display login page with error
            //console.log(errors.array());
            res.render('login', { title: 'Connexion - TravelExpress', auth: req.auth, loginErrors: errors.array({ onlyFirstError: true }), authError, req});
        } else {
            //OK!
            req.session.auth = true;
            req.session.userId = user._id;
            // Go back to main page
            res.redirect('/');
        }
    } catch (error) {
        console.error(error);
        next(error);
    }
});

router.get('/signup', function (req, res, next) {
    res.render('login', { title: 'Inscription - TravelExpress', auth: req.auth });
});

router.post('/signup', [
    body('firstName')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer votre prénom")
        .isLength({ min: 1, max: 30 })
        .withMessage("Votre prénom doit comporter entre 1 et 30 caractères"),
    body('lastName')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer votre nom")
        .isLength({ min: 1, max: 30 })
        .withMessage("Votre nom doit comporter entre 1 et 30 caractères"),
    body('mail')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer votre adresse courriel")
        .isEmail()
        .withMessage("Veuillez indiquer une adresse courriel valide")
        .bail()
        .custom(async (value) => {
            var user = await UserService.getUserByMail(value);

            if(user !== null) return Promise.reject();
        })
        .withMessage("Cette adresse courriel est déjà utiliée sur le site"),
    body('phone')
        .isMobilePhone()
        .withMessage("Veuillez indiquer un numéro de téléphone valide"),
    body('password')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez indiquer un mot de passe")
        .isLength({ min: 5 })
        .withMessage("Le mot de passe doit contenir au moins 5 caractères"),
    body('passwordConfirm')
        .exists({ checkFalsy: true })
        .withMessage("Veuillez saisir votre mot de passe une deuxième fois")
        .if(body('password').exists({ checkFalsy: true }).isLength({ min: 5 }))
        .custom((value, { req }) => value === req.body.password)
        .withMessage("Les mots de passe ne correspondent pas"),
], async function (req, res) {
    try {
        const errors = validationResult(req);

        if(errors.isEmpty()) {
            // Create user, then log in as this user
            var createdUser = await UserService.addUser(req.body.firstName, req.body.lastName, req.body.mail, req.body.password, req.body.phone);
            req.session.auth = true;
            req.session.userId = createdUser._id;
            res.redirect('/');
        } else {
            // Display form with errors
            res.render('login', { title: 'Inscription - TravelExpress', auth: req.auth, signupErrors: errors.array({ onlyFirstError: true }), reqSignup: req});
        }
    } catch(error) {
        console.log(error);
        next(error);
    }
});

router.get('/logout', function (req, res) {
    req.session.auth = false;
    req.session.userId = null;

    res.redirect('/');
});


module.exports = router;
