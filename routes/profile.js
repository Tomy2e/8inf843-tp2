var express = require('express');
var router = express.Router();
var denyAnonymousMiddleware = require('../middlewares/deny-anonymous');
const { param, body, validationResult } = require('express-validator');
var UserService = require('../services/user');
var ObjectId = require('mongoose').Types.ObjectId;
var CarService = require('../services/car');
var TravelService = require('../services/travel');


// If user is not authenticated, redirect to home page
router.use(denyAnonymousMiddleware);

/* GET home page. */
router.get('/', function (req, res, next) {
  
  var errors = [];
  if(req.query.success === "0"){
    errors.push({msg:"Image invalide"});
  }
  //console.log("test"+req.query.success + req.query);

  res.render('profile', { title: 'Mon profil - TravelExpress', errors: errors,auth: req.auth, user: req.user, selectedPage: 'profile' ,success: (req.query.success  === "1")});
});

router.post('/', [
  body('firstName')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer votre prénom")
    .isLength({ min: 1, max: 30 })
    .withMessage("Votre prénom doit comporter entre 1 et 30 caractères"),
  body('lastName')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer votre nom")
    .isLength({ min: 1, max: 30 })
    .withMessage("Votre nom doit comporter entre 1 et 30 caractères"),
  body('mail')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer votre adresse courriel")
    .isEmail()
    .withMessage("Veuillez indiquer une adresse courriel valide")
    .bail()
    .custom(async (value, { req }) => {
      var user = await UserService.getUserByMail(value);
      if (user !== null && user.mail !== req.user.mail) return Promise.reject();
    })
    .withMessage("Cette adresse courriel est déjà utiliée sur le site"),
  body('phone')
    .isMobilePhone()
    .withMessage("Veuillez indiquer un numéro de téléphone valide"),
], async function (req, res, next) {
  try {
    var errors = validationResult(req);

    
    if (errors.isEmpty()) {
      req.user = await UserService.updateUser(req.user, req.body.firstName, req.body.lastName, req.body.phone, req.body.mail);
    }

    res.render('profile', { title: 'Mon profil - TravelExpress', auth: req.auth, user: req.user, selectedPage: 'profile', errors: errors.array({ onlyFirstError: true }) ,success: errors.isEmpty() });
  } catch (error) {
    console.error(error);
    next(error);
  }
});

router.get('/credit', function (req, res) {
  res.render('credit', { title: 'Mes crédits - TravelExpress',user: req.user, auth: req.auth, selectedPage: 'credit', user: req.user });
});

router.post('/credit', [
  body('amount')
    .isNumeric()
    .withMessage("Veuillez entrer une valeur numérique")
    .custom((value) => (value >= 1 && value <= 1000))
    .withMessage("Le montant doit être compris entre 1€ et 1000€"),
], async function (req, res, next) {
  try {
    var errors = validationResult(req);

    if (errors.isEmpty()) {
      req.user = await UserService.addCredit(req.user, req.body.amount);
    }

    res.render('credit', { title: 'Mes crédits - TravelExpress', auth: req.auth, user: req.user, selectedPage: 'credit', errors: errors.array({ onlyFirstError: true }), success: errors.isEmpty() });

  } catch (error) {
    console.error(error);
    next(error);
  }
});

router.get('/cars', async function (req, res) {
  var cars = await CarService.getCars(req.user);
  res.render('addCar', { title: 'Express', selectedPage: 'cars',user: req.user, auth: req.auth, cars, error: (req.query.err == 1) });
});

router.post('/cars', [
  body('model')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer le modèle du véhicule")
    .isLength({ max: 30 })
    .withMessage("Le modèle du véhicule ne peut pas excéder 30 caractères"),
  body('plateNumber')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer le numéro d'immatriculation")
    .isLength({ max: 15 })
    .withMessage("Le numéro d'immatriculation ne peut pas exécder 15 caractères"),
], async function(req, res, next) {
  try {
    var errors = validationResult(req);

    if(errors.isEmpty()) {
      await CarService.addCar(req.user, req.body.model, req.body.plateNumber, 1, false, false, false, false);

      if(req.query.err == 1) {
        res.redirect('/travel/add');
      }
    }

    var cars = await CarService.getCars(req.user);
    res.render('addCar', { title: 'Express', selectedPage: 'cars', auth: req.auth, cars, error: (req.query.err == 1) });
  } catch(error) {
    console.error(error);
    next(error);
  }
});

router.post('/cars/ajax-update/:field', [
  body('id')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer l'ID du véhicule")
    .custom((value) => ObjectId.isValid(value))
    .withMessage("ID véhicule invalide"),
  param('field')
    .isIn(['luggage', 'smoke', 'animals', 'clim', 'bike'])
    .withMessage("Champ incorrect"),
  body('value')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer une valeur"),
], async function (req, res) {
  try {
    var errors = validationResult(req);
    var car = null;
    if (!("id" in errors.mapped())) {
      car = await CarService.getCarById(req.user, req.body.id);
    }

    await body('id')
      .custom(() => car !== null)
      .withMessage("Le véhicule indiqué n'existe pas")
      .run(req);

    if (req.params.field === 'luggage') {
      await body('value')
        .isIn(['1', '2', '3'])
        .withMessage("Veuillez indiquer une valeur égale à 1,2 ou 3")
        .run(req);
    } else {
      await body('value')
        .isIn(['true', 'false'])
        .withMessage("Veuillez indiquer une valeur true/false")
        .run(req);
    }

    errors = validationResult(req);

    if (errors.isEmpty()) {
      var update = {};

      switch (req.params.field) {
        case 'luggage':
          update.luggageSize = Number.parseInt(req.body.value);
          break;

        case 'smoke':
          update.smoke = (req.body.value === "true");
          break;

        case 'animals':
          update.animals = (req.body.value === "true");
          break;

        case 'clim':
          update.clim = (req.body.value === "true");
          break;

        case 'bike':
          update.bike = (req.body.value === "true");
          break;
      }
      await CarService.updateCar(req.user, car, update);

      res.status(200).json({
        success: true,
        data: {},
      });
    } else {
      res.status(400).json({
        success: false,
        errors: errors.mapped(),
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({
      success: false,
      errors: {
        code: "INTERNAL_ERROR",
        message: "Une erreur interne s'est produite, veuillez réessayer plus tard...",
      },
    });
  }
});

router.get('/password', function (req, res) {
  res.render('password', { title: 'Changer mot de passe - TravelExpress',user: req.user, auth: req.auth, selectedPage: 'password' });
});

router.post('/password', [
  body('oldPassword')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer le mot de passe actuel")
    .bail()
    .custom(async (value, { req }) => {
      if (await UserService.checkPassword(req.user, value) === false) {
        return Promise.reject();
      }
    })
    .withMessage("L'ancien mot de passe ne correspond pas"),
  body('password')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez indiquer un mot de passe")
    .isLength({ min: 5 })
    .withMessage("Le mot de passe doit contenir au moins 5 caractères"),
  body('passwordConfirm')
    .exists({ checkFalsy: true })
    .withMessage("Veuillez saisir votre mot de passe une deuxième fois")
    .if(body('password').exists({ checkFalsy: true }).isLength({ min: 5 }))
    .custom((value, { req }) => value === req.body.password)
    .withMessage("Les mots de passe ne correspondent pas"),
], async function (req, res, next) {
  try {
    var errors = validationResult(req);

    if (errors.isEmpty()) {
      await UserService.updatePassword(req.user, req.body.password);
    }

    res.render('password', { title: 'Changer mot de passe - TravelExpress', auth: req.auth, user: req.user, selectedPage: 'password', errors: errors.array({ onlyFirstError: true }), success: errors.isEmpty() });

  } catch (error) {
    console.error(error);
    next(error);
  }
});

router.get('/reservations', async function (req, res, next) {
  try{
    var travels = await TravelService.getTravelToCome(req.user);
    res.render('reservation', { title: 'Mes réservations - TravelExpress',user: req.user, selectedPage: 'reservations', auth: req.auth, travels, TravelService });
  }catch (error) {
    console.error(error);
    next(error);
  }
});

router.get('/history', async function (req, res) {
  try{
    var travels = await TravelService.getTravelHistory(req.user);
    res.render('history', { title: 'Mon Historique - TravelExpress' ,user: req.user,selectedPage: 'history', auth: req.auth, travels, TravelService });
  }catch (error) {
    console.error(error);
    next(error);
  }
});

router.get('/travels', async function (req, res) {
  try{
    var travels = await TravelService.getMyTravel(req.user);
    res.render('travels', { title: 'Mes trajets - TravelExpress' ,user: req.user,selectedPage: 'travels', auth: req.auth, travels, TravelService });
  }catch (error) {
    console.error(error);
    next(error);
  }
});

router.post('/upload', async function (req, res) {
  console.log(req.files.picture.mimetype);
  var i = ['image/png','image/jpeg'].indexOf(req.files.picture.mimetype) > -1 ? true : false;
  i &= req.files.picture.size < 10000000;
  if(i){
    var t = await UserService.updateProfilePic(req.user,req.files.picture);
  }
  res.redirect("/profile?success="+i.toString());
});

module.exports = router;
