var express = require('express');
var router = express.Router();
var CityService = require('../services/city');
var TravelService = require('../services/travel');

const { query, param, validationResult } = require('express-validator');


/* GET home page. */
router.get('/:from/:to/:date', [
  param('date')
  .custom((value) => TravelService.stringToDate(value))
  .bail()
  .custom((value) => {
    var today = new Date();
    today.setHours(0,0,0,0);
    return TravelService.stringToDate(value) >= today;
  })
  .withMessage("Vous ne pouvez pas entrer une date dans le passé")
], async function (req, res, next) {
  try {

    // Get cities : departure / arrival
    var cityDeparture = await CityService.getCityByName(req.params.from);
    var cityArrival = await CityService.getCityByName(req.params.to);

    await param('from')
    .custom(() => cityDeparture !== null)
    .withMessage("La ville de départ est invalide")
    .run(req);

    await param('to')
    .custom(() => cityArrival !== null)
    .withMessage("La ville d'arrivée est invalide")
    .run(req);

    var errors = validationResult(req);

    var travelsFinal = [];

    if(!errors.isEmpty()) {
      // Error
      console.log(errors.array());
    } else {
      // Find all travels
      var travels = await TravelService.findTravels(cityDeparture, cityArrival, TravelService.stringToDate(req.params.date));
      var travelsParsed = [];

      travels.forEach(function(travel) {
        travel = travel.toObject();
        travel.date = TravelService.frenchDateString(travel.departureTime);
        travelsParsed.push(travel);
      });

      // Source: https://stackoverflow.com/a/34890276
      var groupBy = function(xs, key) {
        return xs.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };

      travelsFinal = groupBy(travelsParsed, 'date');
    }

      res.render('search', { title: 'Covoiturage de ' + req.params.from + ' à ' + req.params.to + ' - TravelExpress', auth: req.auth,travels: travelsFinal, req });
  } catch (error) {
    console.error(error);
    next(error);
  }

});

router.get('/city', [
  query('q').exists({ checkFalsy: true }).withMessage("Veuillez fournir une recherche"),
], async function (req, res) {
  var errors = validationResult(req);

  try {
    if (errors.isEmpty()) {
      res.status(200).json({
        success: true,
        data: (await CityService.findCityByName(req.query.q)).sort(function (a, b) {
          var ratioA = req.query.q.length / a.name.length;
          var ratioB = req.query.q.length / b.name.length;
          return ratioB - ratioA;
        }).slice(0, 15),
      });
    } else {
      res.status(400).json({
        success: false,
        errors: {
          code: "QUERY_UNDEFINED",
          message: "Veuillez fournir une recherche"
        },
      });
    }
  } catch (errors) {
    console.error(errors);
    res.status(500).json({
      success: false,
      errors: {
        code: "INTERNAL_ERROR",
        message: "Une erreur interne s'est produite, veuillez réessayer plus tard...",
      },
    });
  }
});

module.exports = router;
