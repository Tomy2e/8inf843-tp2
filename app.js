var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var profileRouter = require('./routes/profile');
var searchRouter = require('./routes/search');
var travelRouter = require('./routes/travel');
var userRouter = require('./routes/user');

var authMiddleware = require('./middlewares/auth');
var verifyCitiesMiddleware = require('./middlewares/verify-cities');

const fileUpload = require('express-fileupload');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(fileUpload());

// Sessions
var sess = {
  secret: 'keyboard cat',
  cookie: {},
  name: 'covoit-session',
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
};

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  //sess.cookie.secure = true; // serve secure cookies
}

app.use(session(sess));

// Check if cities are already imported
app.use(verifyCitiesMiddleware);

// Authentication (using sessions)
app.use(authMiddleware);

// Routes
app.use('/', indexRouter);
app.use('/profile', profileRouter);
app.use('/search', searchRouter);
app.use('/travel', travelRouter);
app.use('/user', userRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
