var Travel = require('../models/travel');
var User = require('../models/user');
var Car = require('../models/car');

class TravelService {
    static addTravel(user, departure, addressDeparture, arrival, addressArrival, indications, departureTime, pricePerPassenger, places, car) {
        return Travel({
            host: user,
            passengers: [],
            departure,
            arrival,
            addressDeparture,
            addressArrival,
            indications,
            departureTime,
            pricePerPassenger,
            places,
            car
        }).save();
    }

    static async addOrUpdatePassenger(user, travel, places) {
        const session = await Travel.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            // Needed to get current passengers
            var travelFound = await Travel.findById(travel._id).populate('passengers.user').session(session).exec();
            if (!travelFound) throw new Error("travel not found");

            // Needed to update credit
            var userFound = await User.findById(user._id).session(session).exec();
            if (!userFound) throw new Error("user not found");

            // Host can't be a passenger
            if(userFound._id.toString() === travelFound.host) throw new Error("Host can't be a passenger");

            var placesReservedBefore = 0;
            var placesReservedNow = places;
            var placesReservedByOthers = 0;

            // Check if update
            var foundReservation = false;
            for (var i = 0; i < travelFound.passengers.length; i++) {
                if (travelFound.passengers[i].user._id.toString() === user._id.toString()) {
                    placesReservedBefore = travelFound.passengers[i].places;
                    foundReservation = true;
                } else {
                    placesReservedByOthers += travelFound.passengers[i].places;
                }
            }

            // Check places
            if ((placesReservedNow + placesReservedByOthers) > travelFound.places) throw new Error("travel capacity exceeded");
            if (placesReservedNow < 0) throw new Error("you must reserve at least one place");

            // Diff places (if positive: give money back, if negative: take money)
            var diffPlaces = placesReservedBefore - placesReservedNow;
            var cost = diffPlaces * travelFound.pricePerPassenger;

            // If negative, check if user has enough credit
            if (cost < 0 && userFound.credit < (cost * -1)) throw new Error("not enough credit");

            // Update travel
            if (foundReservation) {
                if(placesReservedNow === 0) {

                    await Travel.updateOne({
                        _id: travel._id,
                    }, {
                        $pull: {
                            "passengers": {
                                user
                            },
                        }
                    }, opts);
                } else {

                    await Travel.updateOne({
                        _id: travel._id,
                        'passengers.user': user
                    }, {
                        '$set': {
                            "passengers.$.places": placesReservedNow, // https://stackoverflow.com/a/42777522
                        }
                    }, opts);
                }
            } else {
                if(placesReservedNow > 0) {
                    await Travel.updateOne({
                        _id: travel._id,
                    }, {
                        $push: {
                            passengers: {
                                user,
                                places: placesReservedNow
                            }
                        }
                    }, opts);
                }
            }

            // Update user
            await User.updateOne({
                _id: user._id,
            }, {
                $inc: { credit: cost },
            }, opts);

            await session.commitTransaction();
            session.endSession();
            return true;
        } catch (error) {
            // If an error occurred, abort the whole transaction and
            // undo any changes that might have happened
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    }

    static async cancelTravel(user, travel) {
        const session = await Travel.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            // Needed to get current passengers
            var travelFound = await Travel.findOne({
                _id: travel._id,
                host: user,
            }).populate('passengers.user').session(session).exec();
            if (!travelFound) throw new Error("travel not found");

            for (var i = 0; i < travelFound.passengers.length; i++) {
                // Refund each passenger
                await User.updateOne({
                    _id: travelFound.passengers[i].user._id,
                }, {
                    $inc: { credit: travelFound.pricePerPassenger * travelFound.passengers[i].places },
                }, opts);
            }

            await Travel.deleteOne({
                _id: travel._id
            }, opts);

            await session.commitTransaction();
            session.endSession();
            return true;
        } catch (error) {
            // If an error occurred, abort the whole transaction and
            // undo any changes that might have happened
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    }

    static async endTravel(user, travel) {
        const session = await Travel.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            // Needed to get current passengers
            var travelFound = await Travel.findOne({
                _id: travel._id,
                host: user,
            }).populate('passengers.user').session(session).exec();

            if (!travelFound) throw new Error("travel not found");
            if(travelFound.travelDone) throw new Error("this travel has already been ended previously")

            var earnings = 0;
            for (var i = 0; i < travelFound.passengers.length; i++) {
                earnings += travelFound.passengers[i].places * travelFound.pricePerPassenger;
            }

            await User.updateOne({
                _id: user._id,
            }, {
                $inc: { credit: earnings },
            }, opts);

            await Travel.updateOne({
                _id: travel._id,
                host: user
            }, {
                travelDone: true,
            }, opts);

            await session.commitTransaction();
            session.endSession();
            return true;
        } catch (error) {
            // If an error occurred, abort the whole transaction and
            // undo any changes that might have happened
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    }

    static findTravels(departure, arrival, minimalDate) {
        return Travel.find({
            departure,
            arrival,
            travelDone: false,
            departureTime: { $gte: minimalDate },
        })
        .sort('departureTime')
        .populate('car')
        .populate('departure')
        .populate('arrival')
        .exec();
    }

    static getTravelById(id) {
        return Travel.findById(id)
        .populate('car')
        .populate('departure')
        .populate('arrival')
        .populate('host')
        .populate('passengers.user')
        .exec();
    }

    static async getTravelHistory(user){
        return await Travel.find({
            'passengers.user' : user._id,
            travelDone: true,
        })
        .sort('departureTime')
        .populate('departure')
        .populate('arrival')
        .exec();
    }

    static async getTravelToCome(user){
        return await Travel.find({
            'passengers.user' : user._id,
            travelDone: false,
        })
        .sort('departureTime')
        .populate('departure')
        .populate('arrival')
        .exec();
    }

    static async getMyTravel(user){
        return await Travel.find({
            host: user
        })
        .sort('departureTime')
        .populate('departure')
        .populate('arrival')
        .exec();
    }

    static stringToDate(dateStr) {
        var dateSpl = dateStr.split(".");

        if (dateSpl.length !== 3) throw new Error("Date invalide");

        var date = new Date(dateSpl[1] + "." + dateSpl[0] + "." + dateSpl[2]);

        if (isNaN(date.getTime())) throw new Error("Date invalide");

        return date;
    }

    static setTimeFromString(date, timeStr) {
        var parsed = timeStr.split(":");
        if (parsed.length !== 2) throw new Error("Heure invalide");
        console.log('bonjour')
        if (isNaN(parsed[0]) || isNaN(parsed[1])) throw new Error("Heure invalide");

        var hours = Number.parseInt(parsed[0]);
        var minutes = Number.parseInt(parsed[1]);

        if (hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59) {
            date.setHours(hours, minutes);
            return date;
        }

        throw new Error("Heure invalide");
    }

    static frenchDateString(date) {
        var str = "";

        switch (date.getDay()) {
            case 0:
                str += "Dimanche";
                break;
            case 1:
                str += "Lundi";
                break;
            case 2:
                str += "Mardi";
                break;
            case 3:
                str += "Mercredi";
                break;
            case 4:
                str += "Jeudi";
                break;
            case 5:
                str += "Vendredi";
                break;
            case 6:
                str += "Samedi";
                break;
        }

        str += " ";

        str += date.getDate();

        str += " ";

        switch (date.getMonth()) {
            case 0:
                str += "Janvier";
                break;
            case 1:
                str += "Février";
                break;
            case 2:
                str += "Mars";
                break;
            case 3:
                str += "Avril";
                break;
            case 4:
                str += "Mai";
                break;
            case 5:
                str += "Juin";
                break;
            case 6:
                str += "Juillet";
                break;
            case 7:
                str += "Août";
                break;
            case 8:
                str += "Septembre";
                break;
            case 9:
                str += "Octobre";
                break;
            case 10:
                str += "Novembre";
                break;
            case 11:
                str += "Décembre";
                break;
        }

        return str;
    }
}

module.exports = TravelService;