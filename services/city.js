var City = require('../models/city');
var CsvReader = require('promised-csv');
var reader = new CsvReader();

class CityService {
    /**
     * Récupère une ville par son Id
     * @param {ObjectId} id 
     */
    static getCityById(id) {
        return City.findById(id).exec();
    }

    /**
     * Récupère une ville par son nom avec son code postal
     * @param {string} name le nom de la ville
     */
    static getCityByName(name) {
        return City.findOne({
            nameWithPostalCode: name,
        }).exec();
    }

    /**
     * Effectue une recherche de toutes les villes qui correspondent au nom recherché
     * @param {string} name un nom de ville incomplet
     */
    static findCityByName(name) {
        var query = {
            $and: [],
        };
        name.split(" ").forEach(function (keyword) {
            query.$and.push({
                nameWithPostalCode: { $regex: new RegExp(keyword, "i")},
            });
        });
        return City.find(query).exec();
    }

    /**
     * Importe les villes à partir d'un fichier CSV.
     * L'opération n'est pas effectuée si au moins une ville est déjà présente
     * 
     * @param {string} path le chemin vers le fichier CSV
     */
    static async importFromCsvIfNecessary(path) {
        const session = await City.startSession();
        session.startTransaction();
        try {
            await City.createCollection(); // Create collection if it doesn't exist already

            const opts = { session };

            // Find one city randomly
            var cityRandom = await City.findOne().session(session).exec();

            // Import only if no cities in DB
            if(cityRandom == null) {
                // Import cities from CSV file
                var importedCities = [];
                reader.on('row', function (data) {
                    data[8].split("-").forEach(function(postalCode) {
                        importedCities.push(City({
                            name: data[5],
                            postalCode,
                            nameWithPostalCode: data[5] + ' (' + postalCode + ')',
                        }));
                    });

                });
                var cities = await reader.read(path, importedCities);

                // Insert all
                await City.collection.insertMany(cities, opts);
            }

            // Commit
            await session.commitTransaction();
            session.endSession();
            return (cityRandom === null); // Return true if initialized, else false
        } catch (error) {
            // If an error occurred, abort the whole transaction and
            // undo any changes that might have happened
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    }
}

module.exports = CityService;