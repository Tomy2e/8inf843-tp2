var Car = require('../models/car');

class CarService {
    /**
     * Récupère les véhicules ajoutés par un utilisateur
     * 
     * @param {User} user l'utilisateur
     */
    static getCars(user) {
        return Car.find({
            owner: user,
        }).exec();
    }

    static getCarById(user, id) {
        return Car.findOne({
            owner: user,
            _id: id,
        }).exec();
    }

    /**
     * Ajoute un véhicule à un utilisateur
     * 
     * @param {User} user l'utilisateur
     * @param {string} model le modèle du véhicule
     * @param {string} plateNumber le numéro d'immatriculation 
     * @param {number} luggageSize la taille maximale de baggage par passager (1: petit, 2: moyen, 3: grand)
     * @param {Boolean} smoke Autoriser la cigarette ou non
     * @param {number} animals Autoriser les animaux: 1: non, 2: oui
     * @param {Boolean} clim climatisation dans le véhicule
     * @param {Boolean} bike emplacement pour vélo
     */
    static addCar(user, model, plateNumber, luggageSize, smoke, animals, clim, bike) {
        return Car({
            model,
            plateNumber,
            luggageSize,
            smoke,
            animals,
            clim,
            bike,
            owner: user,
        }).save();
    }

    /**
     * Met à jour le véhicule d'un utilisateur
     * 
     * @param {User} user l'utilisateur
     * @param {Car} car le véhicule en question
     * @param {Object} update un object avec les nouvelles valeurs à assigner 
     */
    static updateCar(user, car, update) {
        return Car.updateOne({
            _id: car._id,
            owner: user,
        }, update);
    }

    /**
     * Supprime un véhicule d'un utilisateur
     * 
     * @param {User} user L'utilisateur 
     * @param {Car} car Un de ses véhicules
     */
    static deleteCar(user, car) {
        return deleteOne({
            _id: car._id,
            owner: user,
        });
    }
}

module.exports = CarService;